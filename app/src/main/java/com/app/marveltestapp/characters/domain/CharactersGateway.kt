package com.app.marveltestapp.characters.domain

import com.app.marveltestapp.characters.api.CharactersClient
import com.app.marveltestapp.characters.api.CharactersResponse
import io.reactivex.Single
import javax.inject.Inject

class CharactersGateway @Inject constructor(
    private val charactersClient: CharactersClient
) {

    companion object {
        const val PAGE_SIZE = 15
    }

    fun fetchCharacters(offset: Int): Single<CharactersResponse> =
        charactersClient.fetchCharacters(
            "name",
            PAGE_SIZE,
            offset
        )
}