package com.app.marveltestapp.characters.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.app.marveltestapp.characters.domain.CharactersInteractor
import com.app.marveltestapp.characters.ui.model.Character
import com.app.marveltestapp.common.ui.BaseViewModel
import io.reactivex.Flowable

class CharactersViewModel @ViewModelInject constructor(charactersInteractor: CharactersInteractor): BaseViewModel() {

    val characters: Flowable<PagingData<Character>> =
        charactersInteractor
            .fetchFromDb()
            .cachedIn(viewModelScope)
}