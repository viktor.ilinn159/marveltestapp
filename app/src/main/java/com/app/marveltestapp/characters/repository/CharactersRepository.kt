package com.app.marveltestapp.characters.repository

import androidx.paging.PagingSource
import com.app.marveltestapp.characters.ui.model.Character
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CharactersRepository @Inject constructor(private val charactersDao: CharactersDao) {

    fun insertCharacters(characters: List<Characters.CharactersEntity>) = charactersDao.insertCharacters(characters)

    fun fetchCharacters(): PagingSource<Int, Character> = charactersDao.fetchCharacters()

    fun charactersCount(): Single<Int> = charactersDao.charactersCount()
}