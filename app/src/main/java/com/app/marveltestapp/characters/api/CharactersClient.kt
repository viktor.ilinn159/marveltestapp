package com.app.marveltestapp.characters.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CharactersClient {

    @GET("/v1/public/characters")
    fun fetchCharacters(
        @Query("orderBy") orderBy: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Single<CharactersResponse>
}