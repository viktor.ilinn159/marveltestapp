package com.app.marveltestapp.characters.ui.adapter

import com.app.marveltestapp.characters.ui.model.Character
import com.app.marveltestapp.common.ui.BaseViewHolder
import com.app.marveltestapp.common.ui.executeAfter
import com.app.marveltestapp.databinding.ItemCharacterBinding

class CharacterViewHolder(private val binding: ItemCharacterBinding) : BaseViewHolder<Character>(binding.root) {

    override fun bind(itemToCast: Character) {
        binding.executeAfter {
            character = itemToCast
        }
    }
}