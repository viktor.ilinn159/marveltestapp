package com.app.marveltestapp.characters.ui.model

data class Character(val id: Int, val name: String, val bio: String, val thumbnail: String)
