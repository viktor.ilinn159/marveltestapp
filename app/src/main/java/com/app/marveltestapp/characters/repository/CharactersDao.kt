package com.app.marveltestapp.characters.repository

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.marveltestapp.characters.ui.model.Character
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface CharactersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCharacters(characters: List<Characters.CharactersEntity>): Completable

    @Query("SELECT * FROM characters ORDER BY name")
    fun fetchCharacters(): PagingSource<Int, Character>

    @Query("SELECT COUNT(*) FROM characters")
    fun charactersCount(): Single<Int>
}