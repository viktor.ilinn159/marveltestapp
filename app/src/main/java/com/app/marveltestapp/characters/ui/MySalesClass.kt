package com.app.marveltestapp.characters.ui

class MySalesClass {

    fun buy() {
        val i = 1
        return i * 15000
    }

    fun sell() {
        val i = 100
        return i / 5
    }

    fun count() {
        val a = 55
        return a * 19
    }

}