package com.app.marveltestapp.characters.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.app.marveltestapp.R
import com.app.marveltestapp.characters.ui.adapter.CharactersAdapter
import com.app.marveltestapp.databinding.FragmentCharactersBinding
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable

@AndroidEntryPoint
class CharactersFragment : Fragment() {

    private lateinit var binding: FragmentCharactersBinding

    private val viewModel: CharactersViewModel by viewModels()

    private val adapter by lazy { CharactersAdapter() }

    private val compositeDisposable by lazy { CompositeDisposable() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharactersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        subscribeUI()
    }

    private fun initUI() {
        binding.charactersRecyclerView.adapter = adapter
    }

    private fun subscribeUI() {
        compositeDisposable.add(viewModel.characters
            .subscribe(
                {
                    binding.progressBar.hide()
                    adapter.submitData(lifecycle, it)
                },
                {
                    binding.progressBar.hide()
                    Toast.makeText(context, getString(R.string.error), Toast.LENGTH_LONG).show()
                }
            ))
    }

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
        super.onDestroy()
    }
}