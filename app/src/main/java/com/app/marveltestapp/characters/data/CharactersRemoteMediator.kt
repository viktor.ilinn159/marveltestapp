package com.app.marveltestapp.characters.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxRemoteMediator
import com.app.marveltestapp.characters.api.toEntity
import com.app.marveltestapp.characters.domain.CharactersGateway
import com.app.marveltestapp.characters.repository.Characters
import com.app.marveltestapp.characters.repository.CharactersRepository
import com.app.marveltestapp.characters.ui.model.Character
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@ExperimentalPagingApi
class CharactersRemoteMediator @Inject constructor(
    private val charactersGateway: CharactersGateway,
    private val charactersRepository: CharactersRepository
) : RxRemoteMediator<Int, Character>() {

    override fun initializeSingle(): Single<InitializeAction> {
        return Single.just(InitializeAction.LAUNCH_INITIAL_REFRESH)
    }

    override fun loadSingle(
        loadType: LoadType,
        state: PagingState<Int, Character>
    ): Single<MediatorResult> {
        return charactersRepository
            .charactersCount()
            .flatMap {
                fetchCharacters(it)
            }.flatMap {
                charactersRepository.insertCharacters(it.characters).toSingleDefault(it)
            }.map<MediatorResult> { MediatorResult.Success(endOfPaginationReached = it.endOfPage) }
            .onErrorReturn { MediatorResult.Error(it) }
            .subscribeOn(Schedulers.io())
    }

    private fun fetchCharacters(offset: Int) =
        charactersGateway.fetchCharacters(
            offset = offset
        ).map {
            Characters(
                it.charactersDataResponse.total,
                it.charactersDataResponse.offset,
                it.charactersDataResponse.results.map { it.toEntity() })
        }
}