package com.app.marveltestapp.characters.repository

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.app.marveltestapp.characters.api.CharacterThumbnail

data class Characters(
    val total: Int = 0,
    val offset: Int = 0,
    val characters: List<CharactersEntity>
) {

    val endOfPage = total == offset

    @Entity(tableName = "characters")
    data class CharactersEntity(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,
        val name: String,
        val bio: String,
        val thumbnail: CharacterThumbnail
    )
}
