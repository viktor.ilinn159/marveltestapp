package com.app.marveltestapp.characters.api

import com.app.marveltestapp.characters.repository.Characters
import com.google.gson.annotations.SerializedName

data class CharactersResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("status")
    val status: String,
    @SerializedName("data")
    val charactersDataResponse: CharactersDataResponse
)

data class CharactersDataResponse(
    val offset: Int,
    val limit: Int,
    val total: Int,
    val count: Int,
    val results: List<RemoteCharacter>
)

data class RemoteCharacter(
    val id: Int,
    val name: String,
    val description: String,
    val modified: String,
    val thumbnail: CharacterThumbnail,
    val resourceURI: String
)

fun RemoteCharacter.toEntity() =
    Characters.CharactersEntity(
        name = name,
        bio = description,
        thumbnail = thumbnail
    )

data class CharacterThumbnail(val path: String, val extension: String) {
    override fun toString(): String = "$path.$extension"
}