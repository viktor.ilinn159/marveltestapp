package com.app.marveltestapp.characters.domain

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.app.marveltestapp.characters.data.CharactersRemoteMediator
import com.app.marveltestapp.characters.domain.CharactersGateway.Companion.PAGE_SIZE
import com.app.marveltestapp.characters.repository.CharactersRepository
import com.app.marveltestapp.characters.ui.model.Character
import io.reactivex.Flowable
import javax.inject.Inject

class CharactersInteractor @ExperimentalPagingApi
@Inject constructor(
    private val charactersRepository: CharactersRepository,
    private val remoteMediator: CharactersRemoteMediator
) {

    @OptIn(ExperimentalPagingApi::class)
    fun fetchFromDb(): Flowable<PagingData<Character>> {
        return Pager(
            config = PagingConfig(
                pageSize = PAGE_SIZE,
                enablePlaceholders = true,
                initialLoadSize = PAGE_SIZE * 2),
            remoteMediator = remoteMediator,
            pagingSourceFactory = { charactersRepository.fetchCharacters() }
        ).flowable
    }
}