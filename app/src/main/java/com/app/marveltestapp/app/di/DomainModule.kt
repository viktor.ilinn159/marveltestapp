package com.app.marveltestapp.app.di

import com.app.marveltestapp.common.md5.Md5Hash
import com.app.marveltestapp.common.md5.Md5HashContract
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
abstract class DomainModule {

    @Binds
    abstract fun provideMd5Hash(delegate: Md5Hash): Md5HashContract
}