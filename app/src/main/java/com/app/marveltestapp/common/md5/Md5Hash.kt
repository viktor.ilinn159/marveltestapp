package com.app.marveltestapp.common.md5

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Md5Hash @Inject constructor() : Md5HashContract {

    companion object {
        private const val PUBLIC_API_KEY = "af3f80c398eb2a55e40682be0b007fbd"
        private const val PRIVATE_API_KEY = "07a3fbd0b01b1d992f9a1e44f05af523f48211c7"
    }

    private fun generateHash(strToHash: String): String =
        try {
            val digest = MessageDigest
                .getInstance("MD5")
            digest.update(strToHash.toByteArray())
            val messageDigest = digest.digest()

            val hexString = java.lang.StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2) h = "0$h"
                hexString.append(h)
            }
            hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            ""
        }

    override fun getTs(): String = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()).toString()

    override fun getPublicApiKey(): String = PUBLIC_API_KEY

    override fun getHash(): String = generateHash(getTs().plus(PRIVATE_API_KEY).plus(PUBLIC_API_KEY))
}