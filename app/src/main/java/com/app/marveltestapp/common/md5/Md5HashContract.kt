package com.app.marveltestapp.common.md5

interface Md5HashContract {
    fun getTs(): String
    fun getPublicApiKey(): String
    fun getHash(): String
}