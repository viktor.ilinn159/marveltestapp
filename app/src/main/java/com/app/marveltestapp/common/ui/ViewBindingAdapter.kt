package com.app.marveltestapp.common.ui

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.imageview.ShapeableImageView

@BindingAdapter("loadImage")
fun loadImage(view: ShapeableImageView, res: String?) {
    if (!res.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(res)
            .transition(DrawableTransitionOptions.withCrossFade())
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(view)
    }
}

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.isVisible = visible
}