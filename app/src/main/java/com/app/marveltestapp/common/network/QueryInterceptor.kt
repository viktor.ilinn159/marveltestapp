package com.app.marveltestapp.common.network

import com.app.marveltestapp.common.md5.Md5HashContract
import okhttp3.Interceptor
import okhttp3.Response

class QueryInterceptor(private val md5Hash: Md5HashContract) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val url = original
            .url
            .newBuilder()
            .addQueryParameter("ts", md5Hash.getTs())
            .addQueryParameter("apikey", md5Hash.getPublicApiKey())
            .addQueryParameter("hash", md5Hash.getHash())
            .build()

        val originalRequest = original.newBuilder()
            .url(url).build()

        return chain.proceed(originalRequest)
    }
}