package com.app.marveltestapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.marveltestapp.characters.repository.Characters
import com.app.marveltestapp.characters.repository.CharactersDao
import com.app.marveltestapp.database.converters.CharacterConverter

@Database(entities = [Characters.CharactersEntity::class], version = 1)
@TypeConverters(CharacterConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun charactersDao(): CharactersDao

    companion object {
        const val MARVEL_DATABASE_NAME = "marvel.db"
    }
}