package com.app.marveltestapp.database.di

import android.content.Context
import androidx.room.Room
import com.app.marveltestapp.characters.repository.CharactersDao
import com.app.marveltestapp.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(appContext,
            AppDatabase::class.java,
            AppDatabase.MARVEL_DATABASE_NAME).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideCharactersDao(appDatabase: AppDatabase): CharactersDao {
        return appDatabase.charactersDao()
    }
}