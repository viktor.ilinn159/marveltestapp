package com.app.marveltestapp.database.converters

import androidx.room.TypeConverter
import com.app.marveltestapp.characters.api.CharacterThumbnail

object CharacterConverter {

    @TypeConverter
    fun fromThumbnail(thumbnail: CharacterThumbnail): String = thumbnail.toString()

    @TypeConverter
    fun toThumbnail(thumbnailStr: String) = CharacterThumbnail(path = thumbnailStr.substringBefore("."), thumbnailStr.substringAfter("."))
}