Application that:

1. Uses API (you need to create an account and get an access key).
2. Shows basic information about the found character (name, biography, picture).
3. Stores information about characters already viewed and shows all data about them without access to
network, including after restarting the application.
4. Handles long and erroneous requests.

Stack: Kotlin, RxJava, Retrofit + OkHttp, Room, DI, Glide, MVVM.
Everything is native, no WebView or cross-platform frameworks.
